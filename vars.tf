variable "credentials" {
  #default = "~/workspace/terraform-sa.json"
}

variable "gcp_project_id" {
 #default = "radiant-precept-288303"
}

variable "machine_type" {
  #default     = "n1-standard-1"
}

variable "region" {
  #default = "asia-southeast1"
}

variable "zone" {
  #default = "asia-southeast1-b"
}

variable "gce_network" {
  #default="default"
}

variable "base_image" {
    #default="debien-base-image"
}

variable "startup_script" {
    #default="startup.sh"
}

variable "new_instance_name" {
    #default="startup.sh"
}

variable "instance_name" {
    #default="startup.sh"
}

variable "new_image_name" {
    #default="startup.sh"
}

variable "new_instance_group" {
    
}