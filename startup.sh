sudo apt-get update; sudo apt-get install -yq build-essential python-pip rsync;

x3=$(grep CentOS /etc/os-release | wc -l)
if [ $x3 -gt "0" ]; then rm -f /etc/localtime; ln -s /usr/share/zoneinfo/Asia/Colombo /etc/localtime; service ntpd start; chkconfig firewalld off; setenforce 0; sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config; x1=$(rpm -qa |grep httpd-2 |wc -l); x2=$(rpm -qa |grep supervisor |wc -l); if [ $x1 == $((1)) ]; then service httpd start; chkconfig httpd on; fi; if [ $x2 == $((1)) ]; then service supervisord start; chkconfig supervisord on; fi; fi

x3=$(grep Ubuntu /etc/os-release | wc -l)
if [ $x3 -gt "0" ]; then sudo service ntp start; sudo ufw disable; service apparmor stop; service apparmor teardown; update-rc.d -f apparmor remove; apt-get remove apparmor -y; timedatectl set-timezone Asia/Colombo;  fi