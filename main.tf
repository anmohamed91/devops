provider "google" {
  credentials = var.credentials
  project     = var.gcp_project_id
  region      = var.region
}


resource "google_compute_instance" "default" {
  name = var.new_instance_name
  machine_type = var.machine_type
  zone = var.zone


  tags = ["default-allow-ssh"]

  boot_disk {
    initialize_params {
      image = var.base_image
    }
  }

service_account {
    email = "211012843968-compute@developer.gserviceaccount.com"
    scopes = ["https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring.write","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/trace.append","https://www.googleapis.com/auth/cloud-platform"]
  }

metadata_startup_script=<<SCRIPT
${file("${var.startup_script}")}
SCRIPT

 network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}